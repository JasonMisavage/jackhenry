﻿using Microsoft.AspNetCore.Hosting;
using System;

namespace TwitterStats.API
{
	/// <summary>
	/// Extension methods.
	/// </summary>
	internal static class ExtensionMethods
	{
		/// <summary>
		/// Determines whether the application is being hosted in a development environment.
		/// </summary>
		/// <param name="hostingEnvironment">The hosting environment.</param>
		public static bool IsDevelopment(this IWebHostEnvironment hostingEnvironment) =>
			hostingEnvironment != null &&
			hostingEnvironment.EnvironmentName.Equals("development", StringComparison.InvariantCultureIgnoreCase);
	}
}