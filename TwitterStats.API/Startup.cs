using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TwitterStats.API.Filters;
using TwitterStats.Data;

namespace TwitterStats.API
{
	public class Startup
	{
		private IConfiguration Configuration { get; }

		private IWebHostEnvironment HostingEnvironment { get; }

		/// <summary>
		/// Initializes a new instance of the <see cref="Startup"/> class.
		/// </summary>
		/// <param name="hostingEnvironment">The hosting environment.</param>
		/// <remarks>Called by the runtime.</remarks>
		public Startup(IWebHostEnvironment hostingEnvironment)
		{
			this.HostingEnvironment = hostingEnvironment;
			this.Configuration = new ConfigurationBuilder()
				.SetBasePath(hostingEnvironment.ContentRootPath)
				.AddJsonFile("appsettings.json", false, true)
				.AddJsonFile($"appsettings.{hostingEnvironment.EnvironmentName}.json", false, true)
				.AddEnvironmentVariables()
				.Build();
		}


		/// <summary>
		/// Configures the services.
		/// </summary>
		/// <param name="services">The services.</param>
		/// <remarks>Called by the runtime.</remarks>
		public void ConfigureServices(IServiceCollection services)
		{
			ConfigureSettings(services);
			ConfigureDependencies(services);

			IMvcCoreBuilder mvcBuilder = services.AddMvcCore(config => config.Filters.Add<ApiExceptionFilter>())
				.SetCompatibilityVersion(CompatibilityVersion.Latest)
				.AddControllersAsServices();

			if (this.HostingEnvironment.IsDevelopment())
			{
				mvcBuilder.AddJsonOptions(options => options.JsonSerializerOptions.WriteIndented = true);
			}
		}

		private void ConfigureSettings(IServiceCollection services)
		{

		}

		private void ConfigureDependencies(IServiceCollection services)
		{
			services.AddSingleton<TwitterStreamWatcher>();
			services.AddSingleton<ITweetProcessor, TweetProcessor>();
			services.AddSingleton<IParallelismStrategy, UnboundedParallelismStrategy>();
			services.AddSingleton<IWorkflowStrategy, StatisticsWorkflowStrategy>();
			services.AddSingleton<ActionLibrary>();
			services.AddSingleton<IDataLayer, InMemoryData>();
			services.AddTransient<TweetStatistics>();
		}

		/// <summary>
		/// Configures the HTTP request pipeline.
		/// </summary>
		/// <param name="app">The application.</param>
		/// <param name="env">The environment.</param>
		/// <remarks>Called by the runtime.</remarks>
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseHttpsRedirection();
			app.UseRouting();
			//app.UseAuthorization();
			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}
