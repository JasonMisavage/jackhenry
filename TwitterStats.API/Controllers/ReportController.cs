﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace TwitterStats.API.Controllers
{
	[ApiController, Route("api/report")]
	public class ReportController : ControllerBase
	{
		private readonly TweetStatistics Stats;

		/// <summary>
		/// Initializes a new instance of the <see cref="ReportController"/> class.
		/// </summary>
		/// <param name="stats">The statistics provider.</param>
		public ReportController(TweetStatistics stats) =>
			this.Stats = stats ?? throw new ArgumentNullException(nameof(stats));


		[HttpGet]
		public IActionResult Get()
		{
			List<ReportLine> output = new List<ReportLine>();

			output.Add(new ReportLine()
			{
				Label = "Total tweets received",
				Value = $"{this.Stats.GetTotalTweets()}"
			});

			output.Add(new ReportLine()
			{
				Label = "Average tweets per hour over the last 24 hours",
				Value = this.Stats.GetAveragePerHour(24).ToString("0.00")
			});

			output.Add(new ReportLine()
			{
				Label = "Average tweets per minute over the last 60 minutes",
				Value = this.Stats.GetAveragePerMinute(60).ToString("0.00")
			});

			output.Add(new ReportLine()
			{
				Label = "Average tweets per second over the last 60 seconds",
				Value = this.Stats.GetAveragePerSecond(60).ToString("0.00")
			});

			output.Add(new ReportLine()
			{
				Label = "Top 10 hashtags",
				Value = string.Join(", ", this.Stats.GetTopHashtags(10))
			});

			output.Add(new ReportLine()
			{
				Label = "Percent of tweets with a URL",
				Value = this.Stats.GetPercentOfTweetsWithURL().ToString("0.00") + "%"
			});

			output.Add(new ReportLine()
			{
				Label = "Top 10 domains",
				Value = string.Join(", ", this.Stats.GetTopDomains(10))
			});

			output.Add(new ReportLine()
			{
				Label = "Percent of tweets with a photo",
				Value = this.Stats.GetPercentOfTweetsWithPhoto().ToString("0.00") + "%"
			});

			output.Add(new ReportLine()
			{
				Label = "Percent of tweets with an emoji",
				Value = this.Stats.GetPercentOfTweetsWithEmoji().ToString("0.00") + "%"
			});

			output.Add(new ReportLine()
			{
				Label = "Top 10 emoji",
				Value = string.Join(", ", this.Stats.GetTopEmoji(10))
			});

			return Ok(output);
		}

		private class ReportLine
		{
			public string Label { get; set; }
			public string Value { get; set; }
		}
	}
}