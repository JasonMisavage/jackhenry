﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace TwitterStats.API.Controllers
{
	[ApiController, Route("api/service")]
	public class ServiceController : ControllerBase
	{
		private TwitterStreamWatcher Watcher { get; }

		/// <summary>
		/// Initializes a new instance of the <see cref="ServiceController"/> class.
		/// </summary>
		/// <param name="watcher">The stream watcher.</param>
		public ServiceController(TwitterStreamWatcher watcher) =>
			this.Watcher = watcher ?? throw new ArgumentNullException(nameof(watcher));


		[HttpGet, Route("start")]
		public IActionResult Start()
		{
			this.Watcher.Start();
			return Ok("Stream watcher started.");
		}

		[HttpGet, Route("stop")]
		public async Task<IActionResult> Stop()
		{
			await this.Watcher.Stop();
			return Ok("Stream watcher stopped.");
		}
	}
}