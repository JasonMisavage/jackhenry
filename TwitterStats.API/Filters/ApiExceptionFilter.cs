﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Threading.Tasks;

namespace TwitterStats.API.Filters
{
	/// <summary>
	/// Last resort handler for exceptions that are not captured elsewhere.
	/// </summary>
	/// <seealso cref="ExceptionFilterAttribute" />
	public class ApiExceptionFilter : ExceptionFilterAttribute
	{
		private readonly IWebHostEnvironment HostingEnvironment;

		/// <summary>
		/// Initializes a new instance of the <see cref="ApiExceptionFilter"/> class.
		/// </summary>
		/// <param name="hostingEnvironment">The hosting environment.</param>
		public ApiExceptionFilter(IWebHostEnvironment hostingEnvironment) =>
			this.HostingEnvironment = hostingEnvironment;

		public override void OnException(ExceptionContext context) => _OnException(context);

		public override Task OnExceptionAsync(ExceptionContext context)
		{
			_OnException(context);
			return Task.CompletedTask;
		}

		private void _OnException(ExceptionContext context)
		{
			// TODO: handle this context.Exception





			// If this is running in the development environment, return without setting properties on 
			// the context so that the developer error page will be shown.
			if (this.HostingEnvironment.IsDevelopment()) { return; }

			context.HttpContext.Response.StatusCode = 500;
			context.Result = new ContentResult()
			{
				Content = "",
				ContentType = "application/json",
				StatusCode = 500
			};
			context.ExceptionHandled = true;
		}
	}
}