﻿using System;
using System.Threading.Tasks.Dataflow;

namespace TwitterStats
{
	/// <summary>
	/// Deteremines the maximum degree of parallelism that the tweet processor 
	/// should use based on the number of cores that the host has.
	/// </summary>
	/// <seealso cref="IParallelismStrategy" />
	public class NumCoresParallelismStrategy : IParallelismStrategy
	{
		/// <summary>
		/// Gets the maximum degree of parallelism.
		/// </summary>
		public int GetMaxParallelism() => Environment.ProcessorCount;
	}

	/// <summary>
	/// Specifies that the underlying task scheduler should manage the 
	/// degree of parallelism for workflow steps.
	/// </summary>
	/// <seealso cref="IParallelismStrategy" />
	public class UnboundedParallelismStrategy : IParallelismStrategy
	{
		/// <summary>
		/// Gets the maximum degree of parallelism.
		/// </summary>
		public int GetMaxParallelism() => DataflowBlockOptions.Unbounded;
	}
}