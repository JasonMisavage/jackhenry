﻿using System;
using System.Threading.Tasks;
using Tweetinvi;
using Tweetinvi.Models;
using Tweetinvi.Streaming;

namespace TwitterStats
{
	/// <summary>
	/// Watches the Twitter API for new tweets.
	/// </summary>
	public class TwitterStreamWatcher : IDisposable
	{
		// These should be in a secret store of some kind, not source controlled.
		private const string ConsumerKey = "IdDpNndq0AOZusYbncmQnGGFL";
		private const string ConsumerSecret = "xKRQRNwy1sB5MA34RkvwhHrJf532KgBD6P05v4kuEnouFqCWJI";
		private const string AccessKey = "1309635099554648065-Hg2fXCcg4vxbn5tUy07Tc8Bwib3p9f";
		private const string AccessSecret = "yPgNDljSdSfxBFk1biqInwatXC0KRSWAKmp5dq9JEV8rZ";

		/// <summary>
		/// Gets or sets the stream of sample tweets.
		/// </summary>
		private ISampleStream SampleStream { get; set; }

		/// <summary>
		/// The tweet processor.
		/// </summary>
		private readonly ITweetProcessor Processor;

		/// <summary>
		/// Initializes a new instance of the <see cref="TwitterStreamWatcher"/> class.
		/// </summary>
		/// <param name="processor">The tweet processor.</param>
		public TwitterStreamWatcher(ITweetProcessor processor) =>
			this.Processor = processor ?? throw new ArgumentNullException(nameof(processor));


		/// <summary>
		/// Starts watching the stream of tweets from Twitter's API.
		/// </summary>
		public void Start()
		{
			// Log in to Twitter (as an app) and get the stream of sample tweets.
			Auth.SetUserCredentials(ConsumerKey, ConsumerSecret, AccessKey, AccessSecret);
			this.SampleStream = Stream.CreateSampleStream();

			// English only just to make it easier to see what's going on.
			this.SampleStream.AddTweetLanguageFilter(LanguageFilter.English);

			// When a tweet is received, throw it over to the processor.
			this.SampleStream.TweetReceived += async (sender, args) =>
				await this.Processor.ProcessAsync(args.Tweet);

			// Annoyingly, StartStream and StartStreamAsync do not return after starting the 
			// stream, they return when StopStream is called. So, fire-and-forget it.
#pragma warning disable CS4014
			Task.Run(() =>
				this.SampleStream.StartStream()
			).ConfigureAwait(false);
#pragma warning restore CS4014
		}

		/// <summary>
		/// Stops watching the stream of tweets from Twitter's API.
		/// </summary>
		public async Task Stop()
		{
			this.SampleStream?.StopStream();
			await this.Processor.StopAsync();
		}

		public void Dispose() => Stop().Wait();
	}
}