﻿namespace TwitterStats
{
	/// <summary>
	/// Contract for classes that use some strategy to determine the maximum
	/// degree of parallelism that the tweet processor should use.
	/// </summary>
	public interface IParallelismStrategy
	{
		/// <summary>
		/// Gets the maximum degree of parallelism.
		/// </summary>
		public int GetMaxParallelism();
	}
}