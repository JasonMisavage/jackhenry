﻿using System.Threading.Tasks.Dataflow;
using Tweetinvi.Models;

namespace TwitterStats
{
	/// <summary>
	/// Contract for classes that set up a processing workflow strategy for tweets.
	/// </summary>
	public interface IWorkflowStrategy
	{
		/// <summary>
		/// Gets the workflow, starting with a buffer.
		/// </summary>
		public BufferBlock<ITweet> GetWorkflow();
	}
}