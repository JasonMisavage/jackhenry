﻿using System;
using System.Collections.Generic;

namespace TwitterStats
{
	/// <summary>
	/// Contract for classes that perform data operations.
	/// </summary>
	public interface IDataLayer
	{
		/// <summary>
		/// Reccords the time that a tweet was created.
		/// </summary>
		/// <param name="time">The time.</param>
		public void AddTweetTime(DateTime time);

		/// <summary>
		/// Gets the recorded times that tweets were created.
		/// </summary>
		public IEnumerable<DateTime> GetTweetTimes();


		/// <summary>
		/// Records an occurrence of a hashtag.
		/// </summary>
		/// <param name="hashtag">The hashtag.</param>
		public void AddHashtag(string hashtag);

		/// <summary>
		/// Gets the hash tag counts.
		/// </summary>
		/// <remarks>
		/// The string is the text of the hashtag and the int is the numebr of times it 
		/// it has been seen. It might make sense to create a new class to replace the 
		/// KeyValuePair[string, int], but for now this will do.
		/// </remarks>
		public IEnumerable<KeyValuePair<string, int>> GetHashTagCounts();


		/// <summary>
		/// Records wheter the tweet contained one or more URLs.
		/// </summary>
		/// <param name="tweetHasUrl"><c>true</c> if the tweet has a URL.</param>
		public void AddURLPresence(bool tweetHasUrl);

		/// <summary>
		/// Gets the number of tweets that did have URLs and the number that did not.
		/// </summary>
		public (int TrueCount, int FalseCount) GetURLPresenceCounts();


		/// <summary>
		/// Records an occurrence of a domain.
		/// </summary>
		/// <param name="domain">The domain.</param>
		public void AddDomain(string domain);

		/// <summary>
		/// Gets the domain counts.
		/// </summary>
		public IEnumerable<KeyValuePair<string, int>> GetDomainCounts();


		/// <summary>
		/// Records wheter the tweet contains a photo.
		/// </summary>
		/// <param name="tweetHasPhoto"><c>true</c> if the tweet has a photo.</param>
		public void AddPhotoPresence(bool tweetHasPhoto);

		/// <summary>
		/// Gets the number of tweets that did have photos and the number that did not.
		/// </summary>
		public (int TrueCount, int FalseCount) GetPhotoPresenceCounts();


		/// <summary>
		/// Records wheter the tweet contains any emoji.
		/// </summary>
		/// <param name="tweetHasEmoji"><c>true</c> if the tweet has one or more emoji.</param>
		public void AddEmojiPresence(bool tweetHasEmoji);

		/// <summary>
		/// Gets the number of tweets that did have emoji and the number that did not.
		/// </summary>
		public (int TrueCount, int FalseCount) GetEmojiPresenceCounts();


		/// <summary>
		/// Records an occurrence of an emoji.
		/// </summary>
		/// <param name="emojiCodepoint">The emoji codepoint.</param>
		public void AddEmoji(string emojiCodepoint);

		/// <summary>
		/// Gets the emoji counts.
		/// </summary>
		public IEnumerable<KeyValuePair<string, int>> GetEmojiCounts();
	}
}