﻿using System.Threading.Tasks;
using Tweetinvi.Models;

namespace TwitterStats
{
	/// <summary>
	/// Contract for classes that process tweets.
	/// </summary>
	public interface ITweetProcessor
	{
		/// <summary>
		/// Gets the number of unprocessed items in the buffer.
		/// </summary>
		public int Count { get; }

		/// <summary>
		/// Processes the specified tweet.
		/// </summary>
		/// <param name="tweet">The tweet.</param>
		public Task ProcessAsync(ITweet tweet);

		/// <summary>
		/// Signals the processor to stop. Returns when all in-progress tasks are done processing.
		/// </summary>
		public Task StopAsync();
	}
}