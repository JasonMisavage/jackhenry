﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace TwitterStats.Data
{
	/// <summary>
	/// In-memory data storage.
	/// </summary>
	/// <seealso cref="IDataLayer" />
	public class InMemoryData : IDataLayer
	{
		private readonly ConcurrentBag<DateTime> TweetCreationTimes = new ConcurrentBag<DateTime>();
		private readonly ConcurrentDictionary<string, int> HashTags = new ConcurrentDictionary<string, int>();
		private readonly ConcurrentDictionary<bool, int> URLPresence = new ConcurrentDictionary<bool, int>();
		private readonly ConcurrentDictionary<string, int> Domains = new ConcurrentDictionary<string, int>();
		private readonly ConcurrentDictionary<bool, int> PhotoPresence = new ConcurrentDictionary<bool, int>();
		private readonly ConcurrentDictionary<bool, int> EmojiPresence = new ConcurrentDictionary<bool, int>();
		private readonly ConcurrentDictionary<string, int> Emojis = new ConcurrentDictionary<string, int>();


		/// <summary>
		/// Initializes a new instance of the <see cref="InMemoryData"/> class.
		/// </summary>
		public InMemoryData()
		{
			this.URLPresence.TryAdd(true, 0);
			this.URLPresence.TryAdd(false, 0);
			this.PhotoPresence.TryAdd(true, 0);
			this.PhotoPresence.TryAdd(false, 0);
			this.EmojiPresence.TryAdd(true, 0);
			this.EmojiPresence.TryAdd(false, 0);
		}


		/// <summary>
		/// Reccords the time that a tweet was created.
		/// </summary>
		/// <param name="time">The time.</param>
		public void AddTweetTime(DateTime time) => this.TweetCreationTimes.Add(time);

		/// <summary>
		/// Gets the recorded times that tweets were created.
		/// </summary>
		public IEnumerable<DateTime> GetTweetTimes() => this.TweetCreationTimes.ToList();


		/// <summary>
		/// Records an occurrence of a hashtag.
		/// </summary>
		/// <param name="hashtag">The hashtag.</param>
		public void AddHashtag(string hashtag)
		{
			this.HashTags.AddOrUpdate(
				hashtag,
				1,
				(key, oldValue) => oldValue + 1
			);
		}

		/// <summary>
		/// Gets the hash tag counts.
		/// </summary>
		/// <returns></returns>
		/// <remarks>
		/// The string is the text of the hashtag and the int is the numebr of times it
		/// it has been seen.
		/// </remarks>
		public IEnumerable<KeyValuePair<string, int>> GetHashTagCounts() =>
			this.HashTags.ToList();


		/// <summary>
		/// Records wheter the tweet contained one or more URLs.
		/// </summary>
		/// <param name="tweetHasUrl"><c>true</c> if the tweet has a URL.</param>
		public void AddURLPresence(bool tweetHasUrl) =>
			this.URLPresence[tweetHasUrl] = this.URLPresence[tweetHasUrl] + 1;

		/// <summary>
		/// Gets the number of tweets that did have URLs and the number that did not.
		/// </summary>
		public (int TrueCount, int FalseCount) GetURLPresenceCounts()
		{
			int trueCount = this.URLPresence[true];
			int falseCount = this.URLPresence[false];
			return (trueCount, falseCount);
		}


		/// <summary>
		/// Records an occurrence of a domain.
		/// </summary>
		/// <param name="domain">The domain.</param>
		public void AddDomain(string domain) =>
			this.Domains.AddOrUpdate(
				domain,
				1,
				(key, oldValue) => oldValue + 1
			);

		/// <summary>
		/// Gets the domain counts.
		/// </summary>
		public IEnumerable<KeyValuePair<string, int>> GetDomainCounts() =>
			this.Domains.ToList();


		/// <summary>
		/// Records wheter the tweet contains a photo.
		/// </summary>
		/// <param name="tweetHasUrl"><c>true</c> if the tweet has a photo.</param>
		public void AddPhotoPresence(bool tweetHasPhoto) =>
			this.PhotoPresence[tweetHasPhoto] = this.PhotoPresence[tweetHasPhoto] + 1;

		/// <summary>
		/// Gets the number of tweets that did have photos and the number that did not.
		/// </summary>
		public (int TrueCount, int FalseCount) GetPhotoPresenceCounts()
		{
			int trueCount = this.PhotoPresence[true];
			int falseCount = this.PhotoPresence[false];
			return (trueCount, falseCount);
		}


		/// <summary>
		/// Records wheter the tweet contains any emoji.
		/// </summary>
		/// <param name="tweetHasUrl"><c>true</c> if the tweet has one or more emoji.</param>
		public void AddEmojiPresence(bool tweetHasEmoji) =>
			this.EmojiPresence[tweetHasEmoji] = this.EmojiPresence[tweetHasEmoji] + 1;

		/// <summary>
		/// Gets the number of tweets that did have emoji and the number that did not.
		/// </summary>
		public (int TrueCount, int FalseCount) GetEmojiPresenceCounts()
		{
			int trueCount = this.EmojiPresence[true];
			int falseCount = this.EmojiPresence[false];
			return (trueCount, falseCount);
		}


		/// <summary>
		/// Records an occurrence of an emoji.
		/// </summary>
		/// <param name="emojiCodepoint">The emoji codepoint.</param>
		public void AddEmoji(string emojiCodepoint)
		{
			this.Emojis.AddOrUpdate(
				emojiCodepoint,
				1,
				(key, oldValue) => oldValue + 1
			);
		}

		/// <summary>
		/// Gets the emoji counts.
		/// </summary>
		public IEnumerable<KeyValuePair<string, int>> GetEmojiCounts() =>
			this.Emojis.ToList();
	}
}