﻿using NeoSmart.Unicode;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TwitterStats
{
	/// <summary>
	/// Provides statistics about the tweets received.
	/// </summary>
	public class TweetStatistics
	{
		private readonly IDataLayer Database;

		private List<DateTime> TweetTimes { get; set; } = null;

		/// <summary>
		/// Initializes a new instance of the <see cref="TweetStatistics"/> class.
		/// </summary>
		/// <param name="database">The database.</param>
		public TweetStatistics(IDataLayer database) =>
			this.Database = database ?? throw new ArgumentNullException(nameof(database));


		public int GetTotalTweets()
		{
			if (this.TweetTimes == null)
			{
				this.TweetTimes = this.Database.GetTweetTimes().ToList();
			}

			return this.TweetTimes.Count;
		}

		public double GetAveragePerHour(int overHours)
		{
			if (overHours < 1) { overHours = 1; }
			if (this.TweetTimes == null)
			{
				this.TweetTimes = this.Database.GetTweetTimes().ToList();
			}

			IEnumerable<DateTime> lastXHours = this.TweetTimes
				.Where(time => time >= DateTime.Now.AddHours(overHours * -1));
			return lastXHours
				.GroupBy(dt => dt.Ticks / TimeSpan.FromHours(1).Ticks)
				.Select(group => group.Count())
				.Average(count => count);
		}

		public double GetAveragePerMinute(int overMinutes)
		{
			if (overMinutes < 1) { overMinutes = 1; }
			if (this.TweetTimes == null)
			{
				this.TweetTimes = this.Database.GetTweetTimes().ToList();
			}

			IEnumerable<DateTime> lastXMinutes = this.TweetTimes
				.Where(time => time >= DateTime.Now.AddMinutes(overMinutes * -1));
			return lastXMinutes
				.GroupBy(dt => dt.Ticks / TimeSpan.FromMinutes(1).Ticks)
				.Select(group => group.Count())
				.Average(count => count);
		}

		public double GetAveragePerSecond(int overSeconds)
		{
			if (overSeconds < 1) { overSeconds = 1; }
			if (this.TweetTimes == null)
			{
				this.TweetTimes = this.Database.GetTweetTimes().ToList();
			}

			IEnumerable<DateTime> lastXSeconds = this.TweetTimes
				.Where(time => time >= DateTime.Now.AddMinutes(overSeconds * -1));
			return lastXSeconds
				.GroupBy(dt => dt.Ticks / TimeSpan.FromSeconds(1).Ticks)
				.Select(group => group.Count())
				.Average(count => count);
		}

		public List<string> GetTopHashtags(int number)
		{
			if (number < 1) { number = 1; }

			return this.Database.GetHashTagCounts()
				.OrderByDescending(kvp => kvp.Value)
				.Take(number)
				.Select(kvp => kvp.Key)
				.ToList();
		}

		public double GetPercentOfTweetsWithURL()
		{
			(int trueCount, int falseCount) = this.Database.GetURLPresenceCounts();
			return ((double)trueCount / (trueCount + falseCount)) * 100;
		}

		public double GetPercentOfTweetsWithPhoto()
		{
			(int trueCount, int falseCount) = this.Database.GetPhotoPresenceCounts();
			return ((double)trueCount / (trueCount + falseCount)) * 100;
		}

		public List<string> GetTopDomains(int number)
		{
			if (number < 1) { number = 1; }

			return this.Database.GetDomainCounts()
				.OrderByDescending(kvp => kvp.Value)
				.Take(number)
				.Select(kvp => kvp.Key)
				.ToList();
		}

		public double GetPercentOfTweetsWithEmoji()
		{
			(int trueCount, int falseCount) = this.Database.GetEmojiPresenceCounts();
			return ((double)trueCount / (trueCount + falseCount)) * 100;
		}

		public IEnumerable<string> GetTopEmoji(int number)
		{
			if (number < 1) { number = 1; }

			List<string> codepoints = this.Database.GetEmojiCounts()
				.OrderByDescending(kvp => kvp.Value)
				.Take(number)
				.Select(kvp => kvp.Key)
				.ToList();

			foreach (string codepoint in codepoints)
			{
				UnicodeSequence emojiSequence = new UnicodeSequence(GetCodepoints(codepoint));
				SingleEmoji emoji = Emoji.All.FirstOrDefault(e => e.Sequence == emojiSequence);
				yield return $"{emoji} ({emoji.Name})";
			}
		}

		private IEnumerable<Codepoint> GetCodepoints(string input)
		{
			string[] codepointNumbers = input.Split("-");
			foreach (string codepointNumber in codepointNumbers)
			{
				uint.TryParse(codepointNumber, out uint value);
				yield return new Codepoint(value);
			}
		}
	}
}