﻿using System;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Tweetinvi.Models;

namespace TwitterStats
{
	/// <summary>
	/// Does what it says on the tin.
	/// </summary>
	/// <seealso cref="ITweetProcessor" />
	public class TweetProcessor : ITweetProcessor
	{
		/*
		The first step in the processing workflow is a message buffer (a queue).
		This allows the ProcessAsync() method below to return as soon as the 
		tweet is in the buffer, without waiting for all the processing.
		Additional processing steps are attached to the buffer with the 
		LinkTo() extension method. This is all set up in the provided 
		IWorkflowStrategy instance.
		See: https://docs.microsoft.com/en-us/dotnet/standard/parallel-programming/dataflow-task-parallel-library?redirectedfrom=MSDN
		*/
		private readonly BufferBlock<ITweet> Workflow;

		/// <summary>
		/// Gets the number of unprocessed items in the buffer.
		/// </summary>
		public int Count => this.Workflow.Count;

		/// <summary>
		/// Initializes a new instance of the <see cref="TweetProcessor"/> class.
		/// </summary>
		public TweetProcessor(IWorkflowStrategy workflow)
		{
			if (workflow == null)
			{
				throw new ArgumentNullException(nameof(workflow));
			}

			this.Workflow = workflow.GetWorkflow();
		}


		/// <summary>
		/// Processes the specified tweet.
		/// </summary>
		/// <param name="tweet">The tweet.</param>
		public async Task ProcessAsync(ITweet tweet)
		{
			if (tweet == null) { return; }
			await this.Workflow.SendAsync(tweet);
		}

		/// <summary>
		/// Signals the processor to stop. Returns when all in-progress tasks are done processing.
		/// </summary>
		public async Task StopAsync()
		{
			this.Workflow.Complete();
			await this.Workflow.Completion;
		}
	}
}