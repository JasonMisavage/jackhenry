﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks.Dataflow;
using Tweetinvi.Models;

namespace TwitterStats
{
	/// <summary>
	/// Provides a workflow that buffers incoming tweets and calculates 
	/// some statistics about them.
	/// </summary>
	/// <remarks>
	/// To scale up in the future, one could create a new 
	/// RemoteWorkerWorkflowStrategy that just pulls a tweet 
	/// off the queue and sends it to a remote machine for processing. 
	/// Each of those remote machines could use this strategy 
	/// for the actual processing.
	/// </remarks>
	/// <seealso cref="IWorkflowStrategy" />
	public class StatisticsWorkflowStrategy : IWorkflowStrategy
	{
		/// <summary>
		/// A block setting indicating the maximum degree of parallelism - meaning the 
		/// max number of messages an action block can process at once.
		/// </summary>
		private readonly ExecutionDataflowBlockOptions Parallelism;

		/// <summary>
		/// An option indicating that completion and fault notifications should 
		/// propegate to linked receivers.
		/// </summary>
		private static readonly DataflowLinkOptions Propegate = new DataflowLinkOptions() { PropagateCompletion = true };

		/// <summary>
		/// The library of actions that will be performed on each tweet.
		/// </summary>
		private readonly ActionLibrary ActionLibrary;

		/// <summary>
		/// Initializes a new instance of the <see cref="StatisticsWorkflowStrategy" /> class.
		/// </summary>
		/// <param name="parallelismStrategy">The parallelism strategy.</param>
		/// <param name="actionLibrary">The library of actions that will be performed on each tweet.</param>
		public StatisticsWorkflowStrategy(IParallelismStrategy parallelismStrategy, ActionLibrary actionLibrary)
		{
			this.ActionLibrary = actionLibrary ?? throw new ArgumentNullException(nameof(actionLibrary));

			if (parallelismStrategy == null)
			{
				throw new ArgumentNullException(nameof(parallelismStrategy));
			}
			this.Parallelism = new ExecutionDataflowBlockOptions()
			{
				MaxDegreeOfParallelism = parallelismStrategy.GetMaxParallelism()
			};
		}


		/// <summary>
		/// Gets the workflow, starting with a buffer.
		/// </summary>
		public BufferBlock<ITweet> GetWorkflow()
		{
			// The starting point of the workflow. Buffers messages from one or more sources.
			// All subsequent steps are executed automatically.
			BufferBlock<ITweet> buffer = new BufferBlock<ITweet>();

			/*
			From here, processing branches out into five categories: 
			1) Stats about the number of tweets
			2) Stats about emojis
			3) Stats about hashtags
			4) Stats about URLs
			5) Stats about images
			Use a BroadcastBlock to get those five workflows going at once.
			*/
			BroadcastBlock<ITweet> broadcaster = new BroadcastBlock<ITweet>(tweet => tweet);

			// Send messages from the buffer to the broadcaster...
			buffer.LinkTo(broadcaster, Propegate);

			/// ...and from the broadcaster to the five workflow branches.
			broadcaster.LinkTo(GetTweetNumberWorkflowBranch(), Propegate);
			broadcaster.LinkTo(GetEmojiWorkflowBranch(), Propegate);
			broadcaster.LinkTo(GetHashtagWorkflowBranch(), Propegate);
			broadcaster.LinkTo(GetUrlWorkflowBranch(), Propegate);
			broadcaster.LinkTo(GetImagesWorkflowBranch(), Propegate);

			return buffer;
		}

		private ITargetBlock<ITweet> GetTweetNumberWorkflowBranch()
		{
			return new ActionBlock<ITweet>(
				this.ActionLibrary.RecordTime,
				this.Parallelism
			);
		}

		private ITargetBlock<ITweet> GetEmojiWorkflowBranch()
		{
			TransformBlock<ITweet, List<string>> getEmojiTransform =
				new TransformBlock<ITweet, List<string>>(
					this.ActionLibrary.GetEmoji,
					this.Parallelism
				);

			BroadcastBlock<List<string>> broadcaster =
				new BroadcastBlock<List<string>>(emojiList => emojiList);
			getEmojiTransform.LinkTo(broadcaster, Propegate);

			// Record whether or not the tweet had any emoji.
			ActionBlock<List<string>> recordEmojiPresence =
				new ActionBlock<List<string>>(
					this.ActionLibrary.RecordWheterTweetHasEmoji,
					this.Parallelism
				);
			broadcaster.LinkTo(recordEmojiPresence, Propegate);

			// Record which emoji(s?) the tweet had.
			TransformManyBlock<List<string>, string> foreachInListTransform =
				new TransformManyBlock<List<string>, string>(
					(list) => list,
					this.Parallelism
				);
			broadcaster.LinkTo(foreachInListTransform, Propegate);

			ActionBlock<string> recordEmoji = new ActionBlock<string>(
				this.ActionLibrary.RecordEmoji,
				this.Parallelism
			);
			foreachInListTransform.LinkTo(recordEmoji, Propegate);

			return getEmojiTransform;
		}

		private ITargetBlock<ITweet> GetHashtagWorkflowBranch()
		{
			TransformManyBlock<ITweet, string> hashtagTransform =
				new TransformManyBlock<ITweet, string>(
					this.ActionLibrary.GetHashTags,
					this.Parallelism
				);

			ActionBlock<string> recordHashtag =
				new ActionBlock<string>(
					this.ActionLibrary.RecordHashTag,
					this.Parallelism
				);

			hashtagTransform.LinkTo(recordHashtag, Propegate);
			return hashtagTransform;
		}

		private ITargetBlock<ITweet> GetUrlWorkflowBranch()
		{
			BroadcastBlock<ITweet> broadcaster = new BroadcastBlock<ITweet>(tweet => tweet);

			// Record whether the tweet has one or more URLs.
			ActionBlock<ITweet> recordURLs = new ActionBlock<ITweet>(
				this.ActionLibrary.RecordWheterTweetHasURL,
				this.Parallelism
			);
			broadcaster.LinkTo(recordURLs, Propegate);

			// Find all the URLs in the tweet...
			TransformManyBlock<ITweet, string> urlTransform =
				new TransformManyBlock<ITweet, string>(
					this.ActionLibrary.GetURLs,
					this.Parallelism
				);
			broadcaster.LinkTo(urlTransform, Propegate);

			// ... pull out their domains...
			TransformBlock<string, string> domainTransform =
				new TransformBlock<string, string>(
					this.ActionLibrary.GetDomain,
					this.Parallelism
				);
			urlTransform.LinkTo(domainTransform, Propegate);

			// ... and record them.
			ActionBlock<string> recordDomain = new ActionBlock<string>(
				this.ActionLibrary.RecordDomain,
				this.Parallelism
			);
			domainTransform.LinkTo(recordDomain, Propegate);

			return broadcaster;
		}

		private ITargetBlock<ITweet> GetImagesWorkflowBranch()
		{
			return new ActionBlock<ITweet>(
				this.ActionLibrary.RecordWheterTweetHasAPhoto,
				this.Parallelism
			);
		}
	}
}