﻿using NeoSmart.Unicode;
using System;
using System.Collections.Generic;
using System.Linq;
using Tweetinvi.Models;

namespace TwitterStats
{
	/// <summary>
	/// The library of things that we can do to a tweet.
	/// </summary>
	/// <remarks>
	/// All these methods are here to 1) de-clutter the StatisticsWorkflowStrategy
	/// class a bit, 2) make them available for (potential) other workflow strategies and
	/// 3) make them all easy to test in isolation.
	/// </remarks>
	public class ActionLibrary
	{
		private readonly IDataLayer DataLayer;
		private readonly List<string> AllEmojiCodepoints;

		/// <summary>
		/// Initializes a new instance of the <see cref="ActionLibrary"/> class.
		/// </summary>
		/// <param name="dataLayer">The data layer.</param>
		public ActionLibrary(IDataLayer dataLayer)
		{
			this.DataLayer = dataLayer ?? throw new ArgumentNullException(nameof(dataLayer));
			this.AllEmojiCodepoints = Emoji.All
				.Select(e => string.Join("-", e.Sequence.Codepoints.Select(cp => cp.AsUtf32)))
				.ToList();
		}


		/// <summary>
		/// A placeholder that does nothing.
		/// </summary>
		/// <param name="tweet">The tweet.</param>
		public void NullAction(ITweet tweet) { }

		/// <summary>
		/// Records the (local) time the tweet was posted. This is used 
		/// for counting tweets and for finding average rates.
		/// </summary>
		/// <param name="tweet">The tweet.</param>
		public void RecordTime(ITweet tweet)
		{
			if (tweet == null) { return; }

			DateTime time = tweet.CreatedAt;
			this.DataLayer.AddTweetTime(time);
		}

		/// <summary>
		/// Gets the collection of hashtags from a tweet.
		/// </summary>
		/// <param name="tweet">The tweet.</param>
		public IEnumerable<string> GetHashTags(ITweet tweet) => tweet?.Hashtags?
			.Select(tag => tag.Text)
			.Where(tag => !string.IsNullOrEmpty(tag));

		/// <summary>
		/// Records the hashtag.
		/// </summary>
		/// <param name="hashtag">The hashtag.</param>
		public void RecordHashTag(string hashtag)
		{
			if (string.IsNullOrWhiteSpace(hashtag)) { return; }

			// Keep everything lower-case so #Stuff is the same as #stuff.
			string htText = hashtag.ToLowerInvariant().Trim();

			this.DataLayer.AddHashtag(htText);
		}

		/// <summary>
		/// Records whether the tweet has one or more URLs.
		/// </summary>
		/// <param name="tweet">The tweet.</param>
		public void RecordWheterTweetHasURL(ITweet tweet)
		{
			if (tweet == null) { return; }
			bool hasUrl = tweet.Urls?.Any() ?? false;
			this.DataLayer.AddURLPresence(hasUrl);
		}

		/// <summary>
		/// Gets all the expanded URLs in the tweet.
		/// </summary>
		/// <param name="tweet">The tweet.</param>
		public IEnumerable<string> GetURLs(ITweet tweet) =>
			tweet?.Urls?
				.Where(url => url != null)
				.Select(url => url.ExpandedURL)
				.Where(url => !string.IsNullOrWhiteSpace(url));

		/// <summary>
		/// Gets the domain of the given URL, or null if the URL is invalid.
		/// </summary>
		/// <param name="url">The URL.</param>
		public string GetDomain(string url)
		{
			try
			{
				Uri uri = new Uri(url);
				return uri.Host.ToLowerInvariant();
			}
			catch
			{
				return null;
			}
		}

		/// <summary>
		/// Records the domain name.
		/// </summary>
		/// <param name="domain">The domain name.</param>
		public void RecordDomain(string domain)
		{
			if (string.IsNullOrWhiteSpace(domain)) { return; }
			this.DataLayer.AddDomain(domain);
		}

		/// <summary>
		/// Records the wheter the tweet has a photo.
		/// </summary>
		/// <param name="tweet">The tweet.</param>
		public void RecordWheterTweetHasAPhoto(ITweet tweet)
		{
			if (tweet == null) { return; }

			bool hasPhoto = tweet.Media?
				.Select(x => x.DisplayURL.ToLowerInvariant())
				.Where(x => x.Contains("pic.twitter.com") || x.Contains("instagram"))
				.Any() ?? false;

			this.DataLayer.AddPhotoPresence(hasPhoto);
		}

		/// <summary>
		/// From the given tweet, get the characters that represent an emoji as a set
		/// of one or more UTF32 codepoints.
		/// </summary>
		/// <param name="tweet">The tweet.</param>
		public List<string> GetEmoji(ITweet tweet)
		{
			if (tweet == null) { return new List<string>(); }

			List<string> textCodepoints = tweet.FullText?
				.Letters()
				.Select(letter => string.Join("-", letter.Codepoints().Select(cp => cp.AsUtf32)))
				.ToList();

			// Zip the list of all emoji codepoints and the list of codepoints 
			// in the tweet together. 
			List<string> matches = this.AllEmojiCodepoints.Join(
				textCodepoints,
				available => available, text => text,
				(available, text) => text
			).ToList() ?? new List<string>();

			return matches;
		}

		/// <summary>
		/// Records the wheter the tweet has any emoji.
		/// </summary>
		/// <param name="tweet">The tweet.</param>
		public void RecordWheterTweetHasEmoji(List<string> emojiList)
		{
			emojiList = emojiList ?? new List<string>();
			bool hasAnyEmoji = emojiList.Any();
			this.DataLayer.AddEmojiPresence(hasAnyEmoji);
		}

		/// <summary>
		/// Records the emoji.
		/// </summary>
		/// <param name="emojiCodepoint">The emoji codepoint.</param>
		public void RecordEmoji(string emojiCodepoint)
		{
			if (string.IsNullOrWhiteSpace(emojiCodepoint)) { return; }
			this.DataLayer.AddEmoji(emojiCodepoint);
		}
	}
}