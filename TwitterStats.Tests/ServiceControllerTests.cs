﻿using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace TwitterStats.Tests
{
	public class ServiceControllerTests
	{
		[Fact]
		public async void CanCallWatcherStart()
		{
			using SimulatedServer server = new SimulatedServer();

			HttpResponseMessage result = await server.Client.GetAsync("api/service/start");
			Assert.True(result.IsSuccessStatusCode);

			Assert.Equal(
				"Stream watcher started.",
				await result.Content.ReadAsStringAsync()
			);
		}

		[Fact]
		public async void CanCallWatcherStop()
		{
			using SimulatedServer server = new SimulatedServer();
			HttpResponseMessage result = await server.Client.GetAsync("api/service/stop");
			Assert.True(result.IsSuccessStatusCode);

			Assert.Equal(
				"Stream watcher stopped.",
				await result.Content.ReadAsStringAsync()
			);
		}

		[Fact]
		public async void QuickAndDirtyEndToEndTest()
		{
			using SimulatedServer server = new SimulatedServer();
			await server.Client.GetAsync("api/service/start");

			// Wait a few seconds for some tweets to pour in.
			await Task.Delay(10_000);

			// Stop the service.
			await server.Client.GetAsync("api/service/stop");


			// Then verify that we actually recorded some stats.
			IDataLayer database = server.GetService<IDataLayer>();

			// Tweet times.
			Assert.True(database.GetTweetTimes().Any());

			// Hashtags.
			Assert.True(database.GetHashTagCounts().Any());

			// URL presence.
			(int urlTrueCount, int urlFalseCount) = database.GetURLPresenceCounts();
			Assert.True(urlTrueCount > 0);
			Assert.True(urlFalseCount > 0);

			// Domains.
			Assert.True(database.GetDomainCounts().Any());

			// Photo presence.
			(int photoTrueCount, int photoFalseCount) = database.GetPhotoPresenceCounts();
			Assert.True(photoTrueCount > 0);
			Assert.True(photoFalseCount > 0);

			// Emoji presence.
			(int emojiTrueCount, int emojiFalseCount) = database.GetEmojiPresenceCounts();
			Assert.True(emojiTrueCount > 0);
			Assert.True(emojiFalseCount > 0);

			// Emoji.
			Assert.True(database.GetEmojiCounts().Any());
		}
	}
}