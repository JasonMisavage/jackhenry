﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Net.Http;
using TwitterStats.API;

namespace TwitterStats.Tests
{
	/*
    "Could not find Whatever.deps.json..."
    If you get an error like the one above while trying to use this class in a test, add NuGet references
    for the Microsoft.AspNetCore.App meta-package and the Microsoft.AspNetCore.Mvc.Testing package to 
    your test project.
    */

	/// <summary>
	/// Creates a simulated web server with the same setup as the real thing. 
	/// Use the Client property to test calls to controller endpoints.
	/// Use the GetService[T] and GetServices[T] methods to get dependency-injected services.
	/// </summary>
	/// <seealso cref="IDisposable" />
	public class SimulatedServer : IDisposable
	{
		private readonly WebApplicationFactory<Startup> Factory;

		/// <summary>
		/// Gets the HTTP client.
		/// </summary>
		public HttpClient Client { get; private set; }

		private TestServer Server { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="SimulatedServer"/> class.
		/// </summary>
		/// <param name="environmentName">The environment name. Defaults to "Development".</param>
		/// <remarks>TODO: Consider an enum for environments.</remarks>
		public SimulatedServer(string environmentName = "Development")
		{
			this.Factory = new WebApplicationFactory<Startup>()
				.WithWebHostBuilder(config => config.UseEnvironment(environmentName));

			this.Server = this.Factory.Server;
			this.Client = this.Factory.CreateClient();
		}


		/// <summary>
		/// Implementation of the service-locator pattern (for shame) to get 
		/// services of type <typeparamref name="TService"/> from the 
		/// dependency injection container.
		/// </summary>
		/// <typeparam name="TService">The type of the service.</typeparam>
		public IEnumerable<TService> GetServices<TService>() => this.Server.Services.GetServices<TService>();

		/// <summary>
		/// Implementation of the service-locator pattern (for shame) to get 
		/// a service of type <typeparamref name="TService"/> from the 
		/// dependency injection container.
		/// </summary>
		/// <typeparam name="TService">The type of the service.</typeparam>
		public TService GetService<TService>() => this.Server.Services.GetService<TService>();

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		public void Dispose()
		{
			this.Client?.Dispose();
			this.Server?.Dispose();
			this.Factory?.Dispose();
		}
	}
}
